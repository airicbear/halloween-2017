﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEdit : MonoBehaviour {

    private bool onCursor;
    private GameObject objToSpawn, objMouse;

	public void SpawnEnemy(GameObject obj) {
        if (!onCursor)
        {
            objToSpawn = obj;
            onCursor = true;
            objMouse = Instantiate(obj, Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0)), transform.rotation);
            objMouse.AddComponent<OnCursor>();
            objMouse.GetComponent<SpriteRenderer>().sortingOrder = 1;
            objMouse.name = "objMouse";
        }
	}

    public void SpawnEnemyAtSpawnPoint(GameObject obj, GameObject spawnPoint)
    {
        GameObject objClone = Instantiate(obj, spawnPoint.transform.position, spawnPoint.transform.rotation);
        objClone.AddComponent<Enemy>();
    }

    public void GivePlayerAmmo(int ammo)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().setAmmo(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().getAmmo() + ammo);
    }

    private void Update()
    {
        if(onCursor && Input.GetButtonDown("Fire1"))
        {
            GameObject objClone = Instantiate(objToSpawn, Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0)), transform.rotation);
            objClone.AddComponent<Enemy>();
            offMouse();
        }
    }

    public void offMouse()
    {
        onCursor = false;
        Destroy(GameObject.Find("objMouse"));
    }

    public bool isOnCursor()
    {
        return onCursor;
    }
}
