﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCursor : MonoBehaviour {
    
	void Update () {
        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
        if(transform.position.z != 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        }
    }
}
