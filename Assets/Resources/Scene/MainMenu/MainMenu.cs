﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    private GameObject player;
    private MainMenuStartup mms;

	public void LoadScene(string scene)
    {
        if(player != null)
        {
            SceneManager.LoadScene(scene);
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void SetPlayer(GameObject player)
   {    
        this.player = player;
        if(player != null && GameObject.Find("ShowCurrentPlayer") != null)
        {
            GameObject.Find("ShowCurrentPlayer").GetComponent<SpriteRenderer>().sprite = player.GetComponent<SpriteRenderer>().sprite;
            mms = GameObject.Find("MainMenuStartup").GetComponent<MainMenuStartup>();
            mms.GetPlayButton().SetActive(true);
        }
    }

    public GameObject GetPlayer()
    {
        return player;
    }
}
