﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnPlayer : MonoBehaviour {
    [SerializeField]
    private GameObject projectile;
    [SerializeField]
    private float projectileDamage;
    [SerializeField]
    private float projectileLifeSpan;
    [SerializeField]
    private int startingAmmo;
    
    private void Awake()
    {
        GameObject player = Instantiate(selectedPlayer(), GameObject.Find("PlayerSpawn").transform.position, GameObject.Find("PlayerSpawn").transform.rotation);
        player.AddComponent<Player>();
        player.tag = "Player";
        player.GetComponent<Player>().setAmmo(startingAmmo);
        player.GetComponent<Player>().setProjectile(projectile);
        player.GetComponent<Player>().setProjectileDamage(projectileDamage);
        player.GetComponent<Player>().setProjectileLifeSpan(projectileLifeSpan);
    }

    public void goToScene(string scene)
    {
        GameObject.FindGameObjectWithTag("Player").transform.position = Vector2.zero;
        SceneManager.LoadScene(scene);
    }

    public GameObject selectedPlayer()
    {
        return ((GameObject)Resources.Load("Scene/MainMenu/X")).GetComponent<MainMenu>().GetPlayer();
    }
}
