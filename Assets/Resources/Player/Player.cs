﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PolygonCollider2D))]
public class Player : MonoBehaviour {
    private GameObject projectile;
    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private int ammo;
    private float projectileDamage;
    private float projectileLifeSpan;
    private float horzPress, vertPress;
    private Rigidbody2D rb2d;
    private Vector3 mousePos;
    private Animator anim;
    private bool facingUp, facingDown, facingLeft, facingRight;
    private GameObject canvas, ammoDisplay;

    private void Awake()
    {
        DontDestroyOnLoad(GameObject.Find("EventSystem"));
        DontDestroyOnLoad(GameObject.Find("SPAWN"));
        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(GameObject.FindGameObjectWithTag("UI"));
        facingDown = true;
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        SetRigidbodyValues();
        Camera.main.transform.SetParent(transform);
    }

    private void Update()
    {
        horzPress = Input.GetAxisRaw("Horizontal");
        vertPress = Input.GetAxisRaw("Vertical");
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) + Vector3.forward * 10;
        FindDirection();
        PlayMovementAnimations();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(Camera.main.GetComponent<GameEdit>().isOnCursor())
            {
                Camera.main.GetComponent<GameEdit>().offMouse();
            }
            else
            {
                Die();
            }
        }
        ammoDisplay = GameObject.Find("AmmoDisplay");
        ammoDisplay.GetComponent<Text>().text = "Ammo: " + ammo;
    }

    private void FixedUpdate()
    {
        Movement();
        if (Input.GetKeyDown(KeyCode.F) && ammo > 0)
        {
            Shoot(projectile);
            ammo--;
        }
    }

    public void Shoot(GameObject projectile)
    {
        GameObject projectileClone = Instantiate(projectile.gameObject, transform.position, transform.rotation);
        projectileClone.AddComponent<Rigidbody2D>();
        projectileClone.GetComponent<Rigidbody2D>().gravityScale = 0;
        projectileClone.AddComponent<Projectile>();
        projectileClone.GetComponent<Projectile>().setProjectileDamage(projectileDamage);
        if (isFacingLeft())
        {
            projectileClone.transform.position += Vector3.left;
            projectileClone.GetComponent<Rigidbody2D>().velocity = Vector2.left * 10;
        }
        if (isFacingRight())
        {
            projectileClone.transform.position += Vector3.right;
            projectileClone.GetComponent<Rigidbody2D>().velocity = Vector2.right * 10;
        }
        if (isFacingDown())
        {
            projectileClone.transform.position += Vector3.down;
            projectileClone.GetComponent<Rigidbody2D>().velocity = Vector2.down * 10;
        }
        if (isFacingUp())
        {
            projectileClone.transform.position += Vector3.up;
            projectileClone.GetComponent<Rigidbody2D>().velocity = Vector2.up * 10;
        }
        Destroy(projectileClone, projectileLifeSpan);
    }

    public void Movement()
    {
        if (isMoving())
        {
            rb2d.velocity = new Vector3(horzPress, vertPress) * speed;
        }
    }

    public bool isMoving()
    {
        return horzPress != 0 || vertPress != 0;
    }
    
    public void PlayMovementAnimations()
    {
        if(isMoving())
        {
            if (facingLeft)
            {
                anim.Play("WalkLeft");
            }
            else if (facingRight)
            {
                anim.Play("WalkRight");
            }
            else if (facingUp)
            {
                anim.Play("WalkUp");
            }
            else if (facingDown)
            {
                anim.Play("WalkDown");
            }
        }
        else
        {
            if (!isMoving() && facingUp)
            {
                anim.Play("IdleUp");
            }
            else if (!isMoving() && facingDown)
            {
                anim.Play("IdleDown");
            }
            else if (!isMoving() && facingRight)
            {
                anim.Play("IdleRight");
            }
            else if (!isMoving() && facingLeft)
            {
                anim.Play("IdleLeft");
            }
        }
    }

    public void FindDirection()
    {
        if (vertPress >= 1)
        {
            facingUp = true;
            facingDown = false;
            facingLeft = false;
            facingRight = false;
        }
        else if (vertPress <= -1)
        {
            facingUp = false;
            facingDown = true;
            facingLeft = false;
            facingRight = false;
        }
        if (horzPress >= 1)
        {
            facingUp = false;
            facingDown = false;
            facingLeft = false;
            facingRight = true;
        }
        else if (horzPress <= -1 )
        {
            facingUp = false;
            facingDown = false;
            facingLeft = true;
            facingRight = false;
        }
    }

    public void moveToCursor(GameObject obj)
    {
        obj.transform.position = mousePos;
    }

    public void setSpeed(float speed)
    {
        this.speed = speed;
    }

    private void SetRigidbodyValues()
    {
        rb2d.drag = 7.5f;
        rb2d.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
        rb2d.interpolation = RigidbodyInterpolation2D.Interpolate;
        rb2d.gravityScale = 0;
    }
    
    public void Die()
    {
        SceneManager.LoadScene("MainMenu");
        Destroy(GameObject.Find("EventSystem"));
        Destroy(GameObject.Find("SPAWN"));
        Destroy(GameObject.FindGameObjectWithTag("UI"));
        Destroy(gameObject);
    }

    public bool isFacingLeft()
    {
        return facingLeft;
    }

    public bool isFacingRight()
    {
        return facingRight;
    }

    public bool isFacingUp()
    {
        return facingUp;
    }

    public bool isFacingDown()
    {
        return facingDown;
    }

    public void setProjectile(GameObject projectile)
    {
        this.projectile = projectile;
    }

    public GameObject getProjectile()
    {
        return projectile;
    }

    public int getAmmo()
    {
        return ammo;
    }

    public void setAmmo(int ammo)
    {
        this.ammo = ammo;
    }

    public void setProjectileDamage(float damage)
    {
        this.projectileDamage = damage;
    }

    public float getProjectileDamage()
    {
        return projectileDamage;
    }

    public void setProjectileLifeSpan(float projectileLifeSpan)
    {
        this.projectileLifeSpan = projectileLifeSpan;
    }

    public float getProjectileLifeSpan()
    {
        return this.projectileLifeSpan;
    }
}
