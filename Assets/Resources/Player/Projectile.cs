﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    [SerializeField]
    private float projectileLifeSpan;
    [SerializeField]
    private float projectileDamage;

    private void Start()
    {
        gameObject.AddComponent<DealDamage>();
        GetComponent<DealDamage>().setDamage(projectileDamage);
    }

    public void setProjectileLifeSpan(float projectileLifeSpan)
    {
        this.projectileLifeSpan = projectileLifeSpan;
    }

    public float getProjectileLifeSpan()
    {
        return this.projectileLifeSpan;
    }

    public void setProjectileDamage(float damage)
    {
        this.projectileDamage = damage;
    }

    public float getProjectileDamage()
    {
        return projectileDamage;
    }
}
