﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cauldron : MonoBehaviour {
    
    public Sprite emptyImg, activatedImg;
    private SpriteRenderer sr;

    public void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

	public void Update () {
        if (touchingPlayer())
        {
            if(Input.GetKeyDown(KeyCode.E))
            {
                emptyCauldron();
            }
        }
	}

    public void emptyCauldron()
    {
        sr.sprite = emptyImg;
    }
    
    public bool isEmpty()
    {
        return sr.sprite == emptyImg;
    }

    public bool touchingPlayer()
    {
        return GetComponent<PolygonCollider2D>().IsTouching(GameObject.FindGameObjectWithTag("Player").GetComponent<PolygonCollider2D>());
    }
}
