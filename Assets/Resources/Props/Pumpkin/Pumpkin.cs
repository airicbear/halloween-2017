﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pumpkin : MonoBehaviour {

    public Sprite offImg, onImg;
    private SpriteRenderer sr;

    public void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    public void Update()
    {
        if (touchingPlayer())
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if(!isOff())
                {
                    turnOff();
                }
                else if(isOff())
                {
                    turnOn();
                }
            }
        }
    }

    public void turnOn()
    {
        sr.sprite = onImg;
    }

    public void turnOff()
    {
        sr.sprite = offImg;
    }

    public bool isOff()
    {
        return sr.sprite == offImg;
    }

    public bool touchingPlayer()
    {
        return GetComponent<PolygonCollider2D>().IsTouching(GameObject.FindGameObjectWithTag("Player").GetComponent<PolygonCollider2D>());
    }
}
