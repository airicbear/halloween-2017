﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Web : MonoBehaviour {

	public void OnTriggerEnter2D(Collider2D c)
    {
        if(c.gameObject.tag == "Player")
        {
            c.GetComponent<Player>().setSpeed(1f);
        }
        if (c.gameObject.tag == "Enemy" && c == c.gameObject.GetComponent<PolygonCollider2D>())
        {
            c.GetComponent<Enemy>().setSpeed(0.75f);
        }
    }

    public void OnTriggerExit2D(Collider2D c)
    {
        if (c.gameObject.tag == "Player")
        {
            c.GetComponent<Player>().setSpeed(5f);
        }
        if (c.gameObject.tag == "Enemy" && c == c.gameObject.GetComponent<PolygonCollider2D>())
        {
            c.GetComponent<Enemy>().setSpeed(2f);
        }
    }
}
