﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour {

    public bool giveAmmo;
    public Sprite closedImg, openImg;
    private SpriteRenderer sr;
    private Animator anim;
    private Player player;
    private PolygonCollider2D chestCollider2D;

    public void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        chestCollider2D = GetComponent<PolygonCollider2D>();
    }

    public void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        if (touchingPlayer())
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (!isClosed())
                {
                    closeChest();
                }
                else if (isClosed())
                {
                    openChest();
                    if(giveAmmo)
                    {
                        player.setAmmo(player.getAmmo() + 5);
                        giveAmmo = false;   
                    }
                }
            }
        }
    }

    public void openChest()
    {
        anim.Play("Open");
    }

    public void closeChest()
    {
        anim.Play("Close");
    }   

    public bool isClosed()
    {
        return sr.sprite == closedImg;
    }

    public bool touchingPlayer()
    {
        PolygonCollider2D playerCollider2D = player.GetComponent<PolygonCollider2D>();
        return this.chestCollider2D.IsTouching(playerCollider2D);
    }
}
