﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torch : MonoBehaviour {

    public bool lit = true;
    private Animator anim;

    public void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void Update()
    {
        if (touchingPlayer())
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (lit)
                {
                    unLight();
                }
                else if (!lit)
                {
                    lightUp();
                }
            }
        }
    }
    
    public bool touchingPlayer()
    {
        return GetComponent<EdgeCollider2D>().IsTouching(GameObject.FindGameObjectWithTag("Player").GetComponent<PolygonCollider2D>());
    }

    public void unLight()
    {
        anim.Play("Unlit");
        lit = false;
    }

    public void lightUp()
    {
        anim.Play("Torch");
        lit = true;
    }
}
