﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField]
    private float health = 100;
    private float maxHealth;
    private float speed = 2;
    private float returnSpeed = 0.5f;
    private bool facingLeft, facingRight, facingUp, facingDown;
    PolygonCollider2D enemyCollider;
    CircleCollider2D detector, attackRange;
    Player player;
    Vector3 startPos;
    Rigidbody2D rb2d;
    Animator anim;
    GameObject healthBar;

    private void Awake()
    {
        // MAX HEALTH
        maxHealth = health;

        // TAG
        this.tag = "Enemy";

        // START POSITION
        startPos = this.transform.position;

        // HEALTHBAR
        healthBar = Instantiate((GameObject) Resources.Load("HealthBar"), (Vector2)this.transform.position + Vector2.up * 0.6f, this.transform.rotation);
        healthBar.transform.SetParent(this.transform);

        // ANIMATOR
        anim = GetComponent<Animator>();

        // PLAYER
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        // RIGIDBODY
        rb2d = gameObject.AddComponent<Rigidbody2D>();
        SetRigidbodyValues();

        // COLLIDER
        enemyCollider = gameObject.AddComponent<PolygonCollider2D>();

        // COLLIDER - DETECTOR
        detector = gameObject.AddComponent<CircleCollider2D>();
        SetDetectorValues();

        // COLLIDER - ATTACK RANGE
        attackRange = gameObject.AddComponent<CircleCollider2D>();

    }

    private void Update()
    {
        if(health <= 0)
        {
            Die();
        }
        if(health > maxHealth)
        {
            health = maxHealth;
        }
        if (transform.position.z != 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        }
        FindDirection();
        PlayMovementAnimations();
        UpdateHealthBar();
    }

    private void FixedUpdate()
    {
        if (isTouchingPlayer())
        {
            player.Die();
        }
        else if (isTriggered())
        {
            transform.position = Vector3.MoveTowards(this.transform.position, player.transform.position, Time.deltaTime * speed);
        }
        else
        {
            transform.position = Vector3.MoveTowards(this.transform.position, startPos, Time.deltaTime * speed * returnSpeed);
        }
    }
    
    public void TakeDamage(float damage)
    {
        health -= damage;
    }

    public void UpdateHealthBar()
    {
        foreach (Transform t in gameObject.transform)
        {
            if (t.name == "HealthBar(Clone)")
            {
                foreach (Transform bar in t)
                {
                    if (bar.name == "Green")
                    {
                        if (bar.localScale.x > 0)
                        {
                            bar.localScale = new Vector2(health/maxHealth, t.localScale.y);
                            bar.localPosition = new Vector3(-getMissingHealth()/maxHealth/2f, 0, 0);
                        }
                    }
                }
            }
        }
    }

    public void PlayMovementAnimations()
    {
        if (transform.position == startPos)
        {
            anim.Play("IdleDown");
        }
        else if (isTriggered() && !isTouchingPlayer())
        {
            if (facingLeft)
            {
                anim.Play("WalkLeft");
            }
            else if (facingRight)
            {
                anim.Play("WalkRight");
            }
            else if (facingUp)
            {
                anim.Play("WalkUp");
            }
            else if (facingDown)
            {
                anim.Play("WalkDown");
            }
        }
        else if(isTouchingPlayer())
        {
            if (facingUp)
            {
                anim.Play("IdleUp");
            }
            else if (facingDown)
            {
                anim.Play("IdleDown");
            }
            else if (facingRight)
            {
                anim.Play("IdleRight");
            }
            else if (facingLeft)
            {
                anim.Play("IdleLeft");
            }
        }
        else if(!isTriggered() && transform.position != startPos)
        {
            if (transform.position.x > startPos.x)
            {
                SetFacingLeft();
                anim.Play("WalkLeft");
            }
            else if(transform.position.x < startPos.x)
            {
                SetFacingRight();
                anim.Play("WalkRight");
            }
            else if(transform.position.y < startPos.y)
            {
                SetFacingUp();
                anim.Play("WalkUp");
            }
            else if(transform.position.y > startPos.y)
            {
                SetFacingDown();
                anim.Play("WalkDown");
            }
        }
    }

    public void SetFacingUp()
    {
        facingUp = true;
        facingDown = false;
        facingLeft = false;
        facingRight = false;
    }

    public void SetFacingDown()
    {
        facingUp = false;
        facingDown = true;
        facingLeft = false;
        facingRight = false;
    }

    public void SetFacingLeft()
    {
        facingUp = false;
        facingDown = false;
        facingLeft = false;
        facingRight = true;
    }
    
    public void SetFacingRight()
    {
        facingLeft = true;
        facingRight = false;
        facingUp = false;
        facingDown = false;
    }

    public void FindDirection()
    {
        if(player.transform.position.y > this.transform.position.y)
        {
            SetFacingUp();
        }
        else if(player.transform.position.y < this.transform.position.y)
        {
            SetFacingDown();
        }
        if(player.transform.position.x > this.transform.position.x)
        {
            SetFacingLeft();
        }
        else if(player.transform.position.x < this.transform.position.x)
        {
            SetFacingRight();
        }
    }

    public void Die()
    {
        GameObject explosionClone = Instantiate((GameObject) Resources.Load("Explosion"), transform.position, transform.rotation);
        Destroy(explosionClone, 0.2f);
        Destroy(this.gameObject);
    }

    public bool isTriggered()
    {
        return detector.IsTouching(player.GetComponent<PolygonCollider2D>());
    }

    public bool isTouchingPlayer()
    {
        return enemyCollider.IsTouching(player.GetComponent<PolygonCollider2D>());
    }

    public void SetDetectorValues()
    {
        detector.radius = 2.75f;
        detector.isTrigger = true;
    }
    
    public void setSpeed(float speed)
    {
        this.speed = speed;
    }

    public float getHealth()
    {
        return health;
    }

    public float getMaxHealth()
    {
        return maxHealth;
    }

    public float getMissingHealth()
    {
        return maxHealth - health;
    }

    public void setHealth(float health)
    {
        this.health = health;
    }

    public GameObject getHealthBar()
    {
        return healthBar;
    }

    public float getReturnSpeed()
    {
        return returnSpeed;
    }

    public void setReturnSpeed(float speed)
    {
        returnSpeed = speed;
    }

    private void SetRigidbodyValues()
    {
        rb2d.drag = 7.5f;
        rb2d.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
        rb2d.interpolation = RigidbodyInterpolation2D.Interpolate;
        rb2d.gravityScale = 0;
    }
}
